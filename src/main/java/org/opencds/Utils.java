package org.opencds;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.hl7.fhir.instance.model.BooleanType;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.CommunicationRequest;
import org.hl7.fhir.instance.model.CommunicationRequest.CommunicationRequestPayloadComponent;
import org.hl7.fhir.instance.model.DateTimeType;
import org.hl7.fhir.instance.model.Duration;
import org.hl7.fhir.instance.model.Encounter;
import org.hl7.fhir.instance.model.HumanName;
import org.hl7.fhir.instance.model.IdType;
import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Medication;
import org.hl7.fhir.instance.model.MedicationDispense.MedicationDispenseDosageInstructionComponent;
import org.hl7.fhir.instance.model.MedicationOrder;
import org.hl7.fhir.instance.model.MedicationOrder.MedicationOrderDispenseRequestComponent;
import org.hl7.fhir.instance.model.MedicationOrder.MedicationOrderDosageInstructionComponent;
import org.hl7.fhir.instance.model.Parameters;
import org.hl7.fhir.instance.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.instance.model.Patient;
import org.hl7.fhir.instance.model.Period;
import org.hl7.fhir.instance.model.PositiveIntType;
import org.hl7.fhir.instance.model.Practitioner;
import org.hl7.fhir.instance.model.ProcedureRequest;
import org.hl7.fhir.instance.model.Reference;
import org.hl7.fhir.instance.model.Resource;
import org.hl7.fhir.instance.model.SimpleQuantity;
import org.hl7.fhir.instance.model.StringType;
import org.hl7.fhir.instance.model.Timing;
import org.hl7.fhir.instance.model.Timing.TimingRepeatComponent;
import org.hl7.fhir.instance.model.Timing.UnitsOfTime;
import org.hl7.fhir.instance.model.Type;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.opencds.common.utilities.AbsoluteTimeDifference;
import org.opencds.common.utilities.DateUtility;
import org.opencds.common.xml.XmlConverter;
import org.opencds.common.xml.XmlEntity;
import org.xml.sax.SAXParseException;

/**
 * 
 *
 */
public class Utils {
	public static boolean isMoreThan12hrs (Object dateTimeDt){
	    DateTimeType dateTime = (DateTimeType) dateTimeDt;

    	DateTime startDateTime = new DateTime(dateTime.getValue());
    	DateTime currentTime = new DateTime();        
		Hours value = Hours.hoursBetween(startDateTime, currentTime);
		if (value.getHours() > 12){
			return true;
		}
		return false;
    }
	
	/**
	 * Returns a Map that contains MedicationOrders mapped to the relevant Meds.
	 * Medications may be in meds OR contained in medicationOrder OR just a code, in which a new Medication with just a code will be created and used.
	 * @param medOrders
	 * @param meds
	 * @return
	 */
	public static Map<MedicationOrder, Medication> getMedOrderToAssociatedMedMap(List<MedicationOrder> medOrders, List<Medication> meds)
	{
		Map<MedicationOrder, Medication> medOrderToMed = new HashMap<MedicationOrder, Medication>();
	
		for (MedicationOrder medOrder : medOrders)
		{
			Medication associatedMed = Utils.getMedReferencedByMedOrder(medOrder, meds);
			if (associatedMed != null)
			{
				medOrderToMed.put(medOrder, associatedMed);
			}
			else
			{
				Medication containedMed = Utils.getMedContainedByMedOrder(medOrder);
				if (containedMed != null)
				{
					medOrderToMed.put(medOrder,  containedMed);
				}
				else
				{
					Medication impliedMed = Utils.getMedImpliedByMedOrderByCode(medOrder);
					if (impliedMed != null)
					{
						medOrderToMed.put(medOrder, impliedMed);
					}
				}
			}
		}
		return medOrderToMed;	
	}
	
	/**
	 * Returns a Map that contains MedicationOrders mapped to the relevant prescribers (Practitioner type).
	 * Prescribers may be in practitioners OR contained in medicationOrder OR just a reference that has no practitoner entry (in Epic, this appears to happen for
	 * meds prescribed by "HISTORICAL, MEDS".  In the last case, if there is a display name, a new Practitioner with just the display name as the HumanName.text 
	 * will be returned.
	 * @param medOrders
	 * @param practitioners
	 * @return
	 */
	public static Map<MedicationOrder, Practitioner> getMedOrderToAssociatedPrescriberMap(List<MedicationOrder> medOrders, List<Practitioner> practitioners)
	{
		Map<MedicationOrder, Practitioner> medOrderToPrescriber = new HashMap<MedicationOrder, Practitioner>();
	
		for (MedicationOrder medOrder : medOrders)
		{
			Practitioner associatedPrescriber = Utils.getPrescriberReferencedByMedOrder(medOrder, practitioners);
			if (associatedPrescriber != null)
			{
				medOrderToPrescriber.put(medOrder, associatedPrescriber);
			}
			else
			{
				Practitioner containedPrescriber = Utils.getPrescriberContainedByMedOrder(medOrder);
				if (containedPrescriber != null)
				{
					medOrderToPrescriber.put(medOrder,  containedPrescriber);
				}
				else
				{
					Practitioner impliedPrescriber = Utils.getPrescriberImpliedByMedOrderReferenceDisplay(medOrder);														
					if (impliedPrescriber != null)
					{
						medOrderToPrescriber.put(medOrder, impliedPrescriber);
					}
				}
			}
		}
		return medOrderToPrescriber;	
	}
	
	/**
	 * Returns medication contained in medOrder, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Medication getMedContainedByMedOrder(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try 
			{
				String medIdFromOrder = ((Reference) medOrder.getMedication()).getReference();	
				if ((medIdFromOrder != null) && (medIdFromOrder.startsWith("#")))
				{
					List<Resource> contained = medOrder.getContained();
					for (Resource containedResource : contained)
					{
						if (containedResource instanceof Medication)
						{
							String containedResourceId = containedResource.getId();
							if ((containedResourceId != null) && (containedResourceId.equals(medIdFromOrder)))
							{
								return (Medication) containedResource;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;		
	}
	
	/**
	 * Returns medication implied by medOrder by its code, or null if not found.  Must be RxNorm; uses first found.
	 * @param medOrder
	 * @return
	 */
	private static Medication getMedImpliedByMedOrderByCode(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try 
			{
				CodeableConcept medCC = medOrder.getMedicationCodeableConcept();
				List<Coding> medCodingList = medCC.getCoding();
				for (Coding medCoding : medCodingList)
				{
					String medCodingSystem = medCoding.getSystem();
					if ((medCodingSystem != null) && (medCodingSystem.equals("http://www.nlm.nih.gov/research/umls/rxnorm")))
					{
						String medCode = medCoding.getCode();
						if (medCode != null)
						{
							Medication impliedMed = new Medication();
							impliedMed.setCode(medCC);
							return impliedMed;
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;		
	}
	
	/**
	 * Looks in meds for the Practitioner prescriber referenced by medOrder.  Returns first one found, or null if not found.
	 * @param medOrder
	 * @param practitioners
	 * @return
	 */
	private static Practitioner getPrescriberReferencedByMedOrder(MedicationOrder medOrder, List<Practitioner> practitioners)
	{
		if ((medOrder != null) && (practitioners != null))
		{
			try 
			{
				String prescriberIdFromOrder = ((Reference) medOrder.getPrescriber()).getReference();	
				
				for (Practitioner practitioner : practitioners)
				{					
					String practitionerId = practitioner.getId();
					
					if ((practitionerId.equals(prescriberIdFromOrder)) || ((practitionerId.contains("Practitioner/")) && (prescriberIdFromOrder.contains(practitionerId)))) // account for reference in medication order that uses absolute path to rest service
					{
						return practitioner;
					}
				}
			} 
			catch (Exception e) 
			{
				// do nothing; may be an expected outcome.
			}
		}
		
		return null;		
	}
	
	/**
	 * Returns prescriber contained in medOrder, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Practitioner getPrescriberContainedByMedOrder(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try 
			{
				String prescriberIdFromOrder = ((Reference) medOrder.getPrescriber()).getReference();	
				if ((prescriberIdFromOrder != null) && (prescriberIdFromOrder.startsWith("#")))
				{
					List<Resource> contained = medOrder.getContained();
					for (Resource containedResource : contained)
					{
						if (containedResource instanceof Practitioner)
						{
							String containedResourceId = containedResource.getId();
							if ((containedResourceId != null) && (containedResourceId.equals(prescriberIdFromOrder)))
							{
								return (Practitioner) containedResource;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;		
	}
	
	/**
	 * Returns prescriber implied by medOrder by its reference displayname, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Practitioner getPrescriberImpliedByMedOrderReferenceDisplay(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try 
			{
				String prescriberDisplayFromOrder = ((Reference) medOrder.getPrescriber()).getDisplay();
				if(prescriberDisplayFromOrder != null)
				{
					Practitioner prescriber = new Practitioner();
					HumanName name = new HumanName();
					name.setText(prescriberDisplayFromOrder);
					prescriber.setName(name);
					return prescriber;
				}				
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;		
	}
	
	/**
	 * Looks in meds for the Medication referenced by medOrder.  Returns first one found, or null if not found.
	 * @param medOrder
	 * @param meds
	 * @return
	 */
	private static Medication getMedReferencedByMedOrder(MedicationOrder medOrder, List<Medication> meds)
	{
		if ((medOrder != null) && (meds != null))
		{
			try 
			{
				String medIdFromOrder = ((Reference) medOrder.getMedication()).getReference();	
				
				for (Medication med : meds)
				{					
					String medId = med.getId();
					
					//System.out.println("MED ID FROM ORDER: " + medIdFromOrder);
					//System.out.println("MED ID: " + medId);
					
					if ((medId.equals(medIdFromOrder)) || ((medId.contains("Medication/")) && (medIdFromOrder.contains(medId)))) // account for reference in medication order that uses absolute path to rest service
					{
						return med;
					}
				}
			} 
			catch (Exception e) 
			{
				// do nothing; may be an expected outcome.
			}
		}
		
		return null;		
	}
	
	/**
	 * Returns all dates between (anchorDate - daysToLooKBack) and anchorDate (inclusive) covered by one of the medication orders.
	 * Returns in ascending order.
	 * Currently only considers medOrders with a repeat that contains at least a period start date.
	 * If specified in input as a desired, and if a parseable dispenseRequest.expectedSupplyDuration is available, uses period start date + expectedSupplyDuration as end date, rather than end date or now (if end date missing). 
	 * E.g., if daysToLookBack = 90, anchorDate is day 0, and starts from 90 days prior.  So can return a maximium of 91 dates, including the anchor date.  
	 * @param useDispenseRequestExpectedSupplyDurationToDetermineEndDate
	 * @param medOrders
	 * @param anchorDate
	 * @param daysToLookBack
	 * @return
	 */
	public static List<Date> getDaysCoveredByMedOrders(boolean useDispenseRequestExpectedSupplyDurationToDetermineEndDate, List<MedicationOrder> medOrders, Date anchorDate, int daysToLookBack)
	{
		HashSet<Date> setToReturn = new HashSet<Date>();
		
		DateUtility dateUtility = DateUtility.getInstance();
		
		if ((medOrders != null) && (anchorDate != null) && (daysToLookBack >= 0))
		{
			ArrayList<Date> potentialDates = new ArrayList<Date>();
			Date anchorDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(anchorDate, "yyyy-MM-dd"), "yyyy-MM-dd");
			for (int k = 0; k < daysToLookBack; k++)
			{
				Date potentialDate = dateUtility.getDateAfterAddingTime(anchorDateWithoutHrsMinSec, Calendar.DATE, -1 * k);
				potentialDates.add(potentialDate);			
			}
			
			for (MedicationOrder medOrder : medOrders)
			{
				List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
				if (dosageInstructionList != null)
				{
					for(MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList)
					{
						Timing timing = dosageInstruction.getTiming();
						if (timing != null)
						{
							TimingRepeatComponent repeat = timing.getRepeat();
							if (repeat != null)
							{
								Period period;
								try 
								{
									period = repeat.getBoundsPeriod();
									// TODO: consider support other types of bounds
									if (period != null)
									{
										Date start = period.getStart();
										Date end = period.getEnd();										
										
										if (start != null)
										{
											Date startDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(start, "yyyy-MM-dd"), "yyyy-MM-dd");
											System.out.println(">startDateWithoutHrsMinsSec: " + startDateWithoutHrsMinSec);
											
											// if desired, attempt to come up with end date based on dispenseRequest.expectedSupplyDuration if available, multiplied by (# of repeats + 1)
											if (useDispenseRequestExpectedSupplyDurationToDetermineEndDate)
											{
												MedicationOrderDispenseRequestComponent dispenseRequest = medOrder.getDispenseRequest();												
												if (dispenseRequest != null)
												{
													int multiplier = 1;
													PositiveIntType numRepeatsAllowedElement = dispenseRequest.getNumberOfRepeatsAllowedElement();
													if (numRepeatsAllowedElement != null)
													{
														multiplier = dispenseRequest.getNumberOfRepeatsAllowed() + 1;
													}
													
													Duration duration = dispenseRequest.getExpectedSupplyDuration();
													if (duration != null)
													{
														BigDecimal durationValue = duration.getValue();
														String durationSystem = duration.getSystem();
														String durationCode = duration.getCode();
														
														if ((durationValue != null) && (durationSystem != null) && (durationCode != null) && (durationSystem.equals("http://unitsofmeasure.org"))) // UCUM required by FHIR if system present, including in Epic.
														{															
															//System.out.println("> Start: " + start);
															if (durationCode.equalsIgnoreCase("d")) // see http://unitsofmeasure.org/ucum.html.  In lower case in Epic.
															{
																end = dateUtility.getDateAfterAddingTime(start, Calendar.DATE, durationValue.intValue() * multiplier);
															}
															else if (durationCode.equalsIgnoreCase("wk"))
															{
																end = dateUtility.getDateAfterAddingTime(start, Calendar.DATE, 7 * durationValue.intValue() * multiplier);
															}
															else if (durationCode.equalsIgnoreCase("mo"))
															{
																end = dateUtility.getDateAfterAddingTime(start, Calendar.MONTH, durationValue.intValue() * multiplier);
															}
															else if (durationCode.equalsIgnoreCase("h"))
															{
																end = dateUtility.getDateAfterAddingTime(start, Calendar.HOUR, durationValue.intValue() * multiplier);
															}
															else if (durationCode.equalsIgnoreCase("min"))
															{
																end = dateUtility.getDateAfterAddingTime(start, Calendar.MINUTE, durationValue.intValue() * multiplier);
															}															
															//System.out.println("> End: " + end);
														}
													}
												}
											}
											
											
											Date endDateWithoutHrsMinSec = null;
											if (end != null)
											{
												endDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(end, "yyyy-MM-dd"), "yyyy-MM-dd");
											}											
													
											for (Date potentialDate : potentialDates)
											{
												if (end == null)
												{
													if (! potentialDate.before(startDateWithoutHrsMinSec))
													{
														setToReturn.add(potentialDate);
													}													
												}
												else
												{																										
													if ((! potentialDate.before(startDateWithoutHrsMinSec)) && (! potentialDate.after(endDateWithoutHrsMinSec)))
													{
														setToReturn.add(potentialDate);
													}													
												}																																								
											}											
										}
									}
								} 
								catch (Exception e) 
								{
									// do nothing
								}									
							}
						}
					}
				}
			}			
		}				
		
		ArrayList<Date> listToReturn = new ArrayList<Date>(setToReturn);
		Collections.sort(listToReturn);
		return listToReturn;
	}
	
	public static boolean systemOutPrintln(String textToDisplay) 
	{		
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
		Date date = new Date();
		System.out.println(dateFormat.format(date) + " " + textToDisplay ); 
		return true;
	}
	
	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed. 
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, String key, String setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<String>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;		
	}
	
	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed. 
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, Integer key, String setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<String>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;		
	}	
	
	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed. 
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, Integer key, Integer setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<Integer>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;		
	}
	
	/**
	 * Searches parameters for the following, and returns map with entries as follows (e.g., entry for "clientLanguage" whose value is a String):
	 * - clientLanguage (String): looks for a parameter with the name "Client Language", case-insensitive, and space-insensitive.  If found, returns that.  If not found and 
	 *   an input global is already defined and is not "", returns that.  Else returns "en-US".
	 * 		Language shall be specifiied as either a 2-character ISO 639-1 language code or a combination of a 2-character ISO 639-1 language code 
	 * 		and a 2-character ISO 3166-1 geographical code, concatenated with a hyphen. Example valid language specifications include: en, en-US, 
	 * 		en-GB, and fr. ISO 639-1 codes are available at http://www.loc.gov/standards/iso639-2/php/English_list.php, and ISO 3166-1 codes are 
	 * 		available at http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm. See Section 5 for normative references to these standards.
	 * - clientTimeZoneOffset (String): looks for a parameter with the name "Client Time Zone Offset", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns null.
	 * 		Time zone offset is from Universal Coordinated Time (UTC). This offset is expressed as +/- hh:mm, e.g., 00:00, -05:00, +07:00. 
	 * 		Note that the client's time zone offset cannot be used to determine a geographical time zone. Unless otherwise specified, all time-stamped 
	 * 		data provided by the client will be assumed to have this time zone offset.	 * 		
	 * - evalTime (DateTimeDt): looks for a parameter with the name "Eval Time", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns current time.
	 *   Eval Time shall be specified without fuzziness, at least down to the date.  
	 * - focalPatientId (IdentifierDt): looks for a parameter with the name "Focal Patient Id", case-insensitive, and space-insensitive.  If found, returns that.  Else returns null. 
	 *	 Should be at least a system and value.  
	 * @param parameters20
	 * @param clientLanguage		global value 
	 * @param clientTimeZoneOffset	global value
	 * @param evalTime				global value
	 */
	public static HashMap<String, Object> getGlobalsFromParameters(Parameters parameters20, String clientLanguage, String clientTimeZoneOffset, Date evalTime)
	{
		HashMap<String, Object> mapToReturn = new HashMap<String, Object>();
		
		boolean clientLanguageFound = false;
		boolean clientTimeZoneOffsetFound = false;
		boolean evalTimeFound = false;
		boolean focalPatientIdFound = false;
		
		if (parameters20 != null)
		{
			List<ParametersParameterComponent> parameterList = parameters20.getParameter();
			for (ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");
					
					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{						
						try
						{
							mapToReturn.put("clientLanguage", ((StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{						
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("EvalTime"))
					{		
						try
						{						
							DateTimeType evalTimeDate = (DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;							
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{												
						try
						{	
							mapToReturn.put("focalPatientId", (Identifier) parameter.getValue());
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}						
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}
		
		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}			
		}
		
		if (! evalTimeFound)
		{			
			if (evalTime != null)
			{
				DateTimeType myOtherTime = new DateTimeType();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeType myDateTime = new DateTimeType(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}
		
		return mapToReturn;
	}
	
	/**
	 * Searches parameters for the following, and returns map with entries as follows (e.g., entry for "clientLanguage" whose value is a String):
	 * - clientLanguage (String): looks for a parameter with the name "Client Language", case-insensitive, and space-insensitive.  If found, returns that.  If not found and 
	 *   an input global is already defined and is not "", returns that.  Else returns "en-US".
	 * 		Language shall be specifiied as either a 2-character ISO 639-1 language code or a combination of a 2-character ISO 639-1 language code 
	 * 		and a 2-character ISO 3166-1 geographical code, concatenated with a hyphen. Example valid language specifications include: �en,� �en-US,� 
	 * 		�en-GB,� and �fr.� ISO 639-1 codes are available at http://www.loc.gov/standards/iso639-2/php/English_list.php, and ISO 3166-1 codes are 
	 * 		available at http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm. See Section 5 for normative references to these standards.
	 * - clientTimeZoneOffset (String): looks for a parameter with the name "Client Time Zone Offset", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns null.
	 * 		Time zone offset is from Universal Coordinated Time (UTC). This offset is expressed as +/- hh:mm, e.g., 00:00, -05:00, +07:00. 
	 * 		Note that the client's time zone offset cannot be used to determine a geographical time zone. Unless otherwise specified, all time-stamped 
	 * 		data provided by the client will be assumed to have this time zone offset.	 * 		
	 * - evalTime (DateTimeDt): looks for a parameter with the name "Eval Time", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns current time.
	 *   Eval Time shall be specified without fuzziness, at least down to the date.  
	 * - focalPatientId (IdentifierDt): looks for a parameter with the name "Focal Patient Id", case-insensitive, and space-insensitive.  If found, returns that.  Else returns null. 
	 *	 Should be at least a system and value.  
	 * @param parameters21
	 * @param clientLanguage		global value 
	 * @param clientTimeZoneOffset	global value
	 * @param evalTime				global value
	 */
	public static HashMap<String, Object> getGlobalsFromParameters21(org.hl7.fhir.dstu3.model.Parameters parameters21, String clientLanguage, String clientTimeZoneOffset, Date evalTime)
	{
		HashMap<String, Object> mapToReturn = new HashMap<String, Object>();
		
		boolean clientLanguageFound = false;
		boolean clientTimeZoneOffsetFound = false;
		boolean evalTimeFound = false;
		boolean focalPatientIdFound = false;
		
		if (parameters21 != null)
		{
			List<org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent> parameterList = parameters21.getParameter();
			for (org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");
					
					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{						
						try
						{
							mapToReturn.put("clientLanguage", ((org.hl7.fhir.dstu3.model.StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{						
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((org.hl7.fhir.dstu3.model.StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}						
					}
					else if ((paramNameNormalized.equalsIgnoreCase("EvalTime")) || (paramNameNormalized.equalsIgnoreCase("evaluateAtDateTime")))
					{		
						try
						{						
							org.hl7.fhir.dstu3.model.DateTimeType evalTimeDate = (org.hl7.fhir.dstu3.model.DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;							
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{												
						try
						{	
							mapToReturn.put("focalPatientId", (org.hl7.fhir.dstu3.model.Identifier) parameter.getValue()); // this mapping may not be right
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}						
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}
		
		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}			
		}
		
		if (! evalTimeFound)
		{			
			if (evalTime != null)
			{
				DateTimeType myOtherTime = new DateTimeType();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeType myDateTime = new DateTimeType();
				myDateTime.setValue(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}
		
		return mapToReturn;
	}

	/**
	 * Expects reason to be sent as a CodeableConcept.  Allows it to be passed from the parent Order.
	 * Returns output Parameters as GAO Parameters.  If being used in CQIF context, expected to be return with the response object as nested parameters.
	 * @param parameters21
	 * @return
	 */

//	public static org.hl7.fhir.dstu3.model.Parameters getGaoEvaluationResult (org.hl7.fhir.instance.model.api.dstu3.DiagnosticOrder diagnosticOrder, org.hl7.fhir.dstu3.model.Order order, org.hl7.fhir.dstu3.model.CodeableConcept reason)
//	{
//		/*
//		 * TODO: this is just an example.  
//		 * Example here: diabetes lab testing
//		 * 
//		 * WE ARE JUST IMPLEMENTING CASE WHERE HgbA1c is being used for Diabetes diagnosis
//		 * 
//		 * Test					Rationale					Appropriateness
//		 * HgbA1c				Diabetes diagnosis			3
//		 * Fasting glucose		Diabetes diagnosis			5
//		 * Random glucose		Diabetes diagnosis			2
//		 * GTT					Diabetes diagnosis			10
//		 * 
//		 * HgbA1c				Diabetes monitoring			10
//		 * Fasting glucose		Diabetes monitoring			8
//		 * Random glucose		Diabetes monitoring			6
//		 * GTT					Diabetes monitoring			2
//		 * 
//		 * Codes expected:		codeSystem			code
//		 * HgbA1c				http://loinc.org 	4548-4			(total in blood)
//		 * Fasting glucose		http://loinc.org	1493-6			(in serum or plasma)
//		 * Random glucose		http://loinc.org	2352-3
//		 * GTT					http://loinc.org	6751-2			(2hr)
//		 * 
//		 * Diabetes diagnosis		opencds			"DiabetesDiagnosis"
//		 * Diabetes monitoring		opencds			"DiabetesMonitoring"
//		 */
//		org.hl7.fhir.dstu3.model.Parameters paramsToReturn = new org.hl7.fhir.dstu3.model.Parameters();
//		if (diagnosticOrder != null)
//		{
//			// assume that the diagnostic order contains a single item with a single code
//			List<DiagnosticOrderItemComponent> itemComponents = diagnosticOrder.getItem();
//			if(itemComponents != null)
//			{
//				DiagnosticOrderItemComponent itemComponent = itemComponents.get(0);
//				if (itemComponent != null)
//				{
//					org.hl7.fhir.dstu3.model.CodeableConcept cc = itemComponent.getCode();
//					if (Utils.isCodingContains3(cc, "http://loinc.org", "4548-4"))
//					{
//						if (reason != null)
//						{
//							if (Utils.isCodingContains3(reason, "opencds", "DiabetesDiagnosis"))
//							{
//								// order
//								ParametersParameterComponent orderParam = new ParametersParameterComponent(new StringType("order"));
//								orderParam.setResource(order);
//								paramsToReturn.addParameter(orderParam);
//								
//								
//								// result
//								ParametersParameterComponent resultParam = new ParametersParameterComponent(new StringType("result"));
//								org.hl7.fhir.dstu3.model.Basic result = new org.hl7.fhir.dstu3.model.Basic();
//								result.setId("1");
//								org.hl7.fhir.dstu3.model.Meta resultMeta = new org.hl7.fhir.dstu3.model.Meta();
//								resultMeta.addProfile("http://hl7.org/fhir/StructureDefinition/gao-result");
//								result.setMeta(resultMeta);
//									// result score
//								org.hl7.fhir.dstu3.model.Extension resultExtensionScore = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionScore.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-score");
//								resultExtensionScore.setValue(new org.hl7.fhir.dstu3.model.DecimalType(3));
//								result.addExtension(resultExtensionScore);
//								
//									// result item
//								org.hl7.fhir.dstu3.model.Extension resultExtensionItem = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionItem.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-item");
//								
//								//org.hl7.fhir.dstu21.model.Meta resultItemMeta = new org.hl7.fhir.dstu21.model.Meta();
//								//resultItemMeta.addProfile("http://hl7.org/fhir/StructureDefinition/gao-extension-item");
//								
//								
//								org.hl7.fhir.dstu3.model.Extension resultExtensionItemCode = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionItemCode.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-item-code"); // TODO: this is a bogus extension URL, needed to not throw an error in HAPI-FHIR
//								org.hl7.fhir.dstu3.model.CodeableConcept itemCodeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding itemCodeCoding = new org.hl7.fhir.dstu3.model.Coding();  
//								itemCodeCoding.setSystem("http://loinc.org");
//								itemCodeCoding.setCode("4548-4");
//								itemCodeCC.addCoding(itemCodeCoding);
//								resultExtensionItemCode.setValue(itemCodeCC);
//								resultExtensionItem.addExtension(resultExtensionItemCode);
//								result.addExtension(resultExtensionItem);
//								
//									// result code
//								org.hl7.fhir.dstu3.model.CodeableConcept resultCodeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding resultCodeCoding = new org.hl7.fhir.dstu3.model.Coding();  
//								resultCodeCoding.setSystem("http://hl7.org/fhir/evaluation-result-code");
//								resultCodeCoding.setCode("outside");
//								resultCodeCC.addCoding(resultCodeCoding);
//								result.setCode(resultCodeCC);
//								
//								resultParam.setResource(result);								
//								paramsToReturn.addParameter(resultParam);								
//								
//								// dss
//								ParametersParameterComponent dssParam = new ParametersParameterComponent(new StringType("dss"));
//								org.hl7.fhir.dstu3.model.Device device = new org.hl7.fhir.dstu3.model.Device();
//								org.hl7.fhir.dstu3.model.CodeableConcept deviceTypeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding deviceTypeCoding = new org.hl7.fhir.dstu3.model.Coding();  
//								deviceTypeCoding.setSystem("opencds");
//								deviceTypeCoding.setCode("OpenCDS");
//								deviceTypeCC.addCoding(deviceTypeCoding);
//								device.setType(deviceTypeCC);
//								dssParam.setResource(device);								
//								paramsToReturn.addParameter(dssParam);								
//							}
//						}
//					}					
//				}
//			}
//		}
//		return paramsToReturn;
	/*	
		
		if (parameters21 != null)
		{
			List<org.hl7.fhir.dstu21.model.Parameters.ParametersParameterComponent> parameterList = parameters21.getParameter();
			for (org.hl7.fhir.dstu21.model.Parameters.ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");
					
					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{						
						try
						{
							mapToReturn.put("clientLanguage", ((org.hl7.fhir.dstu21.model.StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{						
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((org.hl7.fhir.dstu21.model.StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("EvalTime"))
					{		
						try
						{						
							org.hl7.fhir.dstu21.model.DateTimeType evalTimeDate = (DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;							
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}						
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{												
						try
						{	
							mapToReturn.put("focalPatientId", (org.hl7.fhir.dstu21.model.Identifier) parameter.getValue()); // this mapping may not be right
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}						
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}
		
		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}			
		}
		
		if (! evalTimeFound)
		{			
			if (evalTime != null)
			{
				DateTimeDt myOtherTime = new DateTimeDt();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeDt myDateTime = new DateTimeDt(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}
		
		return mapToReturn;
		*/		
//	}
	
	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 * 
	 * Pre-condition: assumes evalTime is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 * 
	 * @param dateTimeDtOrPeriodDt
	 * @param evalTime
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBefore(Type dateTimeDtOrPeriodDt, DateTimeType evalTime, boolean countMaybeAsTrue)
	{
		if ((dateTimeDtOrPeriodDt != null) && (evalTime != null))
		{
			boolean isDateTime = false;
			boolean isPeriod = false;
			DateTimeType dateTimeDt;
			Period periodDt;
			try
			{
				dateTimeDt = (DateTimeType) dateTimeDtOrPeriodDt;
				isDateTime = true;
			}
			catch (Exception e)
			{				
			}
			if (! isDateTime)
			{
				try
				{
					periodDt = (Period) dateTimeDtOrPeriodDt;
					isPeriod = true;
				}
				catch (Exception e)
				{				
				}
			}
			if (isDateTime)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
			else if (isPeriod)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Populates ArrayLists of STU2 Resources (MedicationOrders, Medications, and ProcedureRequests) from Epic BPA Web services CCDA.  Medications are in RxNorm; procedures are in CPT.
	 * @param ccda		Should contain "<?xml version="1.0" encoding="UTF-8"?>"
	 * @param unsignedMedicationOrders
	 * @param referencedMedications
	 * @param unsignedProcedureRequests
	 */
	public static void populateUnsignedMedicationOrdersAndProcedureRequestsFromEpicBpaWebServicesCcda(String ccda, ArrayList<MedicationOrder> unsignedMedicationOrders, ArrayList<Medication> referencedMedications, ArrayList<ProcedureRequest> unsignedProcedureRequests)
	{
		XmlConverter xmlConverter = XmlConverter.getInstance();
		try 
		{
			XmlEntity ccdaEntity = xmlConverter.unmarshalXml(ccda, false, null);
			if (ccdaEntity != null)
			{
				ArrayList<XmlEntity> divEntities = ccdaEntity.getDescendantsWithLabel("div");
				if (divEntities != null)
				{
					for (XmlEntity divEntity : divEntities)
					{
						XmlEntity captionEntity = divEntity.getFirstChildWithLabel("caption");
						if ((captionEntity != null) && (captionEntity.getValue().equals("Unsigned Orders")))
						{
							ArrayList<XmlEntity> itemEntityList = divEntity.getDescendantsWithLabel("item");
							if (itemEntityList != null)
							{
								for (XmlEntity itemEntity : itemEntityList)
								{
									HashMap<String, String> captionToContentMap = getUnsignedOrderCaptionToContentMap(itemEntity);
									// both meds and procedures
									String orderType = captionToContentMap.get("Order Type");
									String orderNumber = captionToContentMap.get("Order Number");
									String orderMode = captionToContentMap.get("Order Mode");
									
									// meds only
									String frequency = captionToContentMap.get("Frequency");
									String route = captionToContentMap.get("Route");
									String dose = captionToContentMap.get("Dose");
									String doseUnits = captionToContentMap.get("Dose Units");
									String startDateStr = captionToContentMap.get("Start Date");
									String endDateStr = captionToContentMap.get("End Date"); // often not present
									String mixtureType = captionToContentMap.get("Mixture Type");
									String preferredRxNormCode = captionToContentMap.get("Preferred RxNorm Code");
									
									Date startDate = null;
									Date endDate = null;
									DateFormat df = new SimpleDateFormat("MM/dd/yy");
									df.setLenient(true);
									try {
										if (startDateStr != null)
										{
											startDate = df.parse(startDateStr);										
										}
										if (endDateStr != null)
										{
											endDate = df.parse(endDateStr);										
										}
									} catch (ParseException e1) {
										e1.printStackTrace();
									}
									
									// procedures only
									String priority = captionToContentMap.get("Priority");
									String cptCode = captionToContentMap.get("CPT Code");
									String resultingAgencyId = captionToContentMap.get("Resulting Agency ID");
									String labTestId = captionToContentMap.get("Lab Test ID");	
									
									// MEDICATIONS
									if ((orderType != null) && (orderType.equals("Medication")) && (preferredRxNormCode != null))
									{
										// create Medication with RxNorm code
										Medication med = new Medication();
										
										IdType medId = IdType.newRandomUuid();
										med.setId(medId);
										
										Coding coding = new Coding().setSystem("http://www.nlm.nih.gov/research/umls/rxnorm").setCode(preferredRxNormCode);										
										CodeableConcept code = new CodeableConcept();
										code.addCoding(coding);										
										med.setCode(code);
										
										// add medication to list
										referencedMedications.add(med);
										
										// create MedicationOrder referencing medication
										MedicationOrder medOrder = new MedicationOrder();
										medOrder.setId(IdType.newRandomUuid());
										medOrder.setMedication(new Reference(med.getId()));
										medOrder.addIdentifier(new Identifier().setSystem("Order Number").setValue(orderNumber));
										
										// dosage instruction
										MedicationOrderDosageInstructionComponent dosageInstruction = new MedicationOrderDosageInstructionComponent();
										if ((dose != null) && (doseUnits != null) && (route != null) && (frequency != null))
										{
											String textDosageInstruction = dose + " " + doseUnits + " " + route + " " + frequency;										
											dosageInstruction.setText(textDosageInstruction);
										}
										
											// frequency
										if (frequency != null)
										{											
											String frequencyUpper = frequency.toUpperCase(); 
											
												// PRN
											boolean prn = false;
											if(frequencyUpper.contains("PRN") || frequencyUpper.contains("AS NEEDED"))
											{
												prn = true;
											}
											dosageInstruction.setAsNeeded(new BooleanType(prn));
											
												// timing
											dosageInstruction.setTiming(getTimingDtFromFreeTextFrequency(frequency, startDate, endDate));
										}
										
											// route
										if (route != null)
										{
											CodeableConcept routeCcDt = new CodeableConcept();
											routeCcDt.setText(route);
											dosageInstruction.setRoute(routeCcDt);
										}
										
											// dose
										if ((dose != null) && (doseUnits != null))
										{
											try
											{
												SimpleQuantity quantityDt = new SimpleQuantity();
										        quantityDt.setValue(BigDecimal.valueOf(Double.parseDouble(dose)));
												quantityDt.setUnit(doseUnits);
												dosageInstruction.setDose(quantityDt);
											}
											catch (Exception e)
											{
												e.printStackTrace();
												System.err.println("Unable to parse dose and add to medication");
											}											
										}
										
										medOrder.addDosageInstruction(dosageInstruction);
										
										// add medication order to list
										unsignedMedicationOrders.add(medOrder);
									}
									// PROCEDURE
									else if ((orderType != null) && (orderType.equals("Procedure")))
									{
										ProcedureRequest procRequest = new ProcedureRequest();
										procRequest.setId(IdType.newRandomUuid());
										procRequest.addIdentifier(new Identifier().setSystem("Order Number").setValue(orderNumber));
										
										// TODO: map priority to FHIR priority enumeration, if needed
										
										CodeableConcept ccDt = new CodeableConcept();
										if (cptCode != null)
										{
											ccDt.addCoding(new Coding().setSystem("http://www.ama-assn.org/go/cpt").setCode(cptCode));
										}
										if (labTestId != null)
										{
											// TODO: see if we can identify a more appropriate system for this.  Corresponded to ARUP test code for sample University of Utah Health case.
											ccDt.addCoding(new Coding().setSystem("http://www.epic.com/lab_test_id").setCode(labTestId));
										}
										procRequest.setCode(ccDt);
										unsignedProcedureRequests.add(procRequest);
									}											
								}								
							}					
						}							
					}
				}
			}
		} 
		catch (SAXParseException e) 
		{	
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Attempt to convert free text frequency to a TimingDt object.  Attempts to populate the "repeat" element with available parameters.  If unable, returns empty object.
	 * 
	 * TODO: this class is currently optimized just for patterns found in office visit ordering of opioids at University of Utah Health.  It should NOT be considered comprehensive/complete.
	 * 
	 * @param freeTextFrequency
	 * @return
	 */
	public static Timing getTimingDtFromFreeTextFrequency(String freeTextFrequency, Date startDate, Date endDate)
	{		
		Timing timingDtToReturn = new Timing();
		TimingRepeatComponent repeat = new TimingRepeatComponent();
		
		if (freeTextFrequency != null)
		{
			String textUpper = freeTextFrequency.toUpperCase();
			
			if (textUpper.startsWith("TWICE WEEKLY"))
			{
				repeat.setFrequency(2);
				repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.WK);						
			}
			else if (textUpper.startsWith("WEEKLY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.WK);				
			}
			else if (textUpper.startsWith("AT BEDTIME"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);				
			}
			else if (textUpper.startsWith("DAILY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);				
			}
			else if (textUpper.startsWith("EVERY EVENING"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);				
			}
			else if (textUpper.startsWith("EVERY NIGHT"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);				
			}
			else if (textUpper.startsWith("EVERY HOUR"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.H);				
			}
			else if (textUpper.startsWith("EVERY OTHER DAY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(2));
				repeat.setPeriodUnits(UnitsOfTime.D);				
			}
			else if (textUpper.startsWith("EVERY"))
			{
				int frequency = getNumberFromString(textUpper);
				
				if (frequency >= 1)
				{
					repeat.setFrequency(frequency);
	                repeat.setPeriod(BigDecimal.valueOf(1));
										
					if (textUpper.contains("MIN") || textUpper.contains("MINUTES"))
					{
						repeat.setPeriodUnits(UnitsOfTime.MIN);						
					}
					else if (textUpper.contains("HOURS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.H);						
					}
					else if (textUpper.contains("DAYS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.D);						
					}
					else if (textUpper.contains("WEEKS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.WK);						
					}					
				}								
			}			
			else if (textUpper.contains("TIMES DAILY") || textUpper.contains("TIMES A DAY") || textUpper.contains("TIMES WEEKLY") || textUpper.contains("TIMES A WEEK"))
			{
				int frequency = getNumberFromString(textUpper);
								
				if (frequency >= 1)
				{
					repeat.setFrequency(frequency);
                    repeat.setPeriod(BigDecimal.valueOf(1));
										
					if (textUpper.contains("TIMES DAILY") || textUpper.contains("TIMES A DAY"))
					{
						repeat.setPeriodUnits(UnitsOfTime.D);
					}
					else if (textUpper.contains("TIMES WEEKLY") || textUpper.contains("TIMES A WEEK"))
					{
						repeat.setPeriodUnits(UnitsOfTime.WK);
					}					
				}
			}			
		}
		
		Period period = new Period();
		if (startDate != null)
		{
			period.setStart(startDate);			
		}
		if (endDate != null)
		{
			period.setEnd(endDate);			
		}
		repeat.setBounds(period);		
		
		timingDtToReturn.setRepeat(repeat);
		
		return timingDtToReturn;
	}
	
	/**
	 * Limited functionality intended to be used only in this class.
	 * First attempts to get int after stripping away all non-numbers from string.
	 * If that doesn't work look for ONE through TEN.
	 * if that doesn't work returns - 1. 
	 * @param string
	 * @return
	 */
	private static int getNumberFromString(String string)
	{
		int numToReturn = -1;
		if (string != null)
		{
			String textUpper = string.toUpperCase();
			String numberString = textUpper.replaceAll("\\D+",""); // strips out all non-numbers
			
			try
			{
				numToReturn = Integer.parseInt(numberString);
			}
			catch (Exception e)
			{
				// expected; ignore
			}
			
			if (numToReturn <= 0) // above didn't work; try to get with word search
			{
				if (textUpper.contains("ONE"))
				{
					numToReturn = 1;
				}
				else if (textUpper.contains("TWO"))
				{
					numToReturn = 2;
				}
				else if (textUpper.contains("TWO"))
				{
					numToReturn = 2;
				}
				else if (textUpper.contains("THREE"))
				{
					numToReturn = 3;
				}
				else if (textUpper.contains("FOUR"))
				{
					numToReturn = 4;
				}
				else if (textUpper.contains("FIVE"))
				{
					numToReturn = 5;
				}
				else if (textUpper.contains("SIX"))
				{
					numToReturn = 6;
				}
				else if (textUpper.contains("SEVEN"))
				{
					numToReturn = 7;
				}
				else if (textUpper.contains("EIGHT"))
				{
					numToReturn = 8;
				}
				else if (textUpper.contains("NINE"))
				{
					numToReturn = 9;
				}
				else if (textUpper.contains("TEN"))
				{
					numToReturn = 10;
				}					
			}
		}
		return numToReturn;		
	}
	
	/**
	 * Returns map of captions to content
	 * @param itemEntity		XML with a repeat of <caption/><content/> enttiies.
	 * @return
	 */
	private static HashMap<String, String> getUnsignedOrderCaptionToContentMap(XmlEntity itemEntity)
	{
		HashMap<String, String> mapToReturn = new HashMap<String, String>();
		ArrayList<XmlEntity> itemChildEntityList = itemEntity.getChildren();
		for (int k = 0; k < itemChildEntityList.size(); k++)
		{			
			mapToReturn.put(itemChildEntityList.get(k).getValue(), itemChildEntityList.get(k + 1).getValue());
		}		
		return mapToReturn;
	}
	
	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 * 
	 * Pre-condition: assumes evalTime is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 * 
	 * @param dateTimeDtOrPeriodDt
	 * @param evalTime
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBefore21(org.hl7.fhir.dstu3.model.Type dateTimeDtOrPeriodDt, org.hl7.fhir.dstu3.model.DateTimeType evalTime, boolean countMaybeAsTrue)
	{
		if ((dateTimeDtOrPeriodDt != null) && (evalTime != null))
		{
			boolean isDateTime = false;
			boolean isPeriod = false;
			org.hl7.fhir.dstu3.model.DateTimeType dateTimeType;
			org.hl7.fhir.dstu3.model.Period period;
			try
			{
				dateTimeType = (org.hl7.fhir.dstu3.model.DateTimeType) dateTimeDtOrPeriodDt;
				isDateTime = true;
			}
			catch (Exception e)
			{				
			}
			if (! isDateTime)
			{
				try
				{
					period = (org.hl7.fhir.dstu3.model.Period) dateTimeDtOrPeriodDt;
					isPeriod = true;
				}
				catch (Exception e)
				{				
				}
			}
			if (isDateTime)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
			else if (isPeriod)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 * 
	 * Pre-condition for now: assumes time2 is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 * Also assume that dateTimeOrPeriod1 is in fact a dateTimeType with a "real" date-time.
	 * TODO: update above pre-condition to be more generic 
	 *
	 * @param dateTimeOrPeriod1
	 * @param time2
	 * @param maxAmountTime1BeforeTime2
	 * @param timeUnits
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBeforeByAtMost21(org.hl7.fhir.dstu3.model.Type dateTimeOrPeriod1, DateTimeType time2, int maxAmountTime1BeforeTime2, int timeUnits, boolean countMaybeAsTrue)
	{
		org.hl7.fhir.dstu3.model.DateTimeType time1 = null;
		try
		{
			time1 = (org.hl7.fhir.dstu3.model.DateTimeType) dateTimeOrPeriod1;
		}
		catch (Exception e)
		{
			return false;
		}
		if ((dateTimeOrPeriod1 != null) && (time2 != null) && (time1 != null))
		{
			java.util.Date date1 = time1.getValue();
			java.util.Date date2 = time2.getValue();
			
			if ((date1 != null) && (date2 != null))
			{
				if (date1.before(date2))
				{
					java.util.Date date2MinusInterval = DateUtility.getInstance().getDateAfterAddingTime(date2, timeUnits, -1 * maxAmountTime1BeforeTime2);

					if (! date1.before(date2MinusInterval))
					{
						return true;
					}
				}
			}
				
		}
		
		return false;
	}
	
	/**
	 * Returns true if the subjectResourceReferenceDt points to the patient and he/she has an identifier that matches the focalPatientId
	 * @param subjectResourceReference
	 * @param patient
	 * @param focalPatientId
	 * @return
	 */
	public static boolean isFocalPatient(Reference subjectResourceReference, Patient patient, Identifier focalPatientId)
	{
		if ((subjectResourceReference != null) && (subjectResourceReference.getReference() != null) && (subjectResourceReference.getReference() != null) && (patient != null) && (patient.getId() != null) && (patient.getId() != null))
		{
			if (patient.getId().equals(subjectResourceReference.getReference()))
			{
				if (Utils.isIdentifierListContains(patient.getIdentifier(), focalPatientId))
				{
					return true;
				}
			}
			
		}
		return false;
	}
	
	/**
	 * Returns true if the subjectResourceReferenceDt points to the patient and he/she has an identifier that matches the focalPatientId
	 * @param subjectResourceReferenceDt
	 * @param patient
	 * @param focalPatientId
	 * @return
	 */
	public static boolean isFocalPatient21(org.hl7.fhir.dstu3.model.Reference subjectResourceReferenceDt, org.hl7.fhir.dstu3.model.Patient patient, org.hl7.fhir.dstu3.model.Identifier focalPatientId)
	{
		if ((subjectResourceReferenceDt != null) && (subjectResourceReferenceDt.getReference() != null) && (subjectResourceReferenceDt.getReference() != null) && (patient != null) && (patient.getId() != null) && (patient.getId() != null))
		{
			if (patient.getId().equals(subjectResourceReferenceDt.getReference()))
			{
				if (Utils.isIdentifierListContains21(patient.getIdentifier(), focalPatientId))
				{
					return true;
				}
			}
			
		}
		return false;
	}
	
	/**
	 * focalPersonId does not need to contain "Patient/"; if not, added before processing.
	 * @param patientReference
	 * @param focalPersonId
	 * @return
	 */
	public static boolean isReferencedPatientIdFocalPersonId(Reference patientReference, String focalPersonId)
	{
		if ((patientReference != null) && (focalPersonId != null))
		{
			if (! focalPersonId.contains("Patient/"))
			{
				focalPersonId = "Patient/" + focalPersonId; // when pulled from Patient, "Patient/" would be added; but otherwise need to add.
			}
			String patientReferenceStr = patientReference.getReference();
			if (patientReferenceStr != null)
			{
				if ((focalPersonId.equals(patientReferenceStr)) || ((focalPersonId.contains("Patient/")) && (patientReferenceStr.contains(focalPersonId)))) // account for reference to patient that uses absolute path to rest service
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if dateTime contained within at least one of medOrder's DosageInstructions.Timing.Repeat.BoundsPeriod
	 * @param dateTime
	 * @param medOrder
	 * @return
	 */
	public static boolean isDateTimeWithinMedicationOrderTimingPeriod(Date dateTime, MedicationOrder medOrder)
	{
		if ((dateTime != null) && (medOrder != null))
		{
			List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
			if (dosageInstructionList != null)
			{
				for (MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList)
				{
					Timing timing = dosageInstruction.getTiming();
					if (timing != null)
					{
						TimingRepeatComponent repeat = timing.getRepeat();
						if (repeat != null)
						{
							// TODO: may need to support other types of bounds
							try 
							{
								Period boundsPeriod = repeat.getBoundsPeriod();
								if (isDateTimeWithinPeriod(dateTime, boundsPeriod))
								{
									return true;
								}
								
							} catch (Exception e) 
							{
								// would not work if not a period							
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if:
	 * - start OR end is present
	 * - if start present, dateTime is on or after start
	 * - if end present, dateTime is on or before end
	 * @param dateTime
	 * @param period
	 * @return
	 */
	public static boolean isDateTimeWithinPeriod(Date dateTime, Period period)
	{
		if ((dateTime != null) && (period != null))
		{
			Date start = period.getStart();
			Date end = period.getEnd();
			
			if ((start != null) || (end != null))
			{
				boolean startNullOrConditionPassed = false;
				boolean endNullOrConditionPassed = false;
				
				if (start == null)
				{
					startNullOrConditionPassed = true;
				}
				else
				{
					if (! dateTime.before(start))
					{
						startNullOrConditionPassed = true;
					}
				}
				
				if (end == null)
				{
					endNullOrConditionPassed = true;
				}
				else
				{
					if (! dateTime.after(end))
					{
						endNullOrConditionPassed = true;
					}
				}
				
				if ((startNullOrConditionPassed) && (endNullOrConditionPassed))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static CommunicationRequest createPayload(String message) 
	{
		CommunicationRequest comRequest = new CommunicationRequest();
		CommunicationRequestPayloadComponent payload = comRequest.addPayload();
		StringType stringType = new  StringType(message);
		payload.setContent(stringType);
		return comRequest;
	}

	public static org.hl7.fhir.dstu3.model.CommunicationRequest createPayload3(String message) 
	{
		org.hl7.fhir.dstu3.model.CommunicationRequest comRequest = new org.hl7.fhir.dstu3.model.CommunicationRequest();
		org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestPayloadComponent payload = comRequest.addPayload();
		org.hl7.fhir.dstu3.model.StringType stringType = new  org.hl7.fhir.dstu3.model.StringType(message);
		payload.setContent(stringType);
		return comRequest;
	}
	
	// function Provenance createProvenance(String message) {
	// 		Provenance prov = new Provenance();
	// 		Agent agent = prov.addAgent();
	// 		IDatatype stringType = new  StringDt(message);
	//         	agent.setDisplay(message);
	// return prov;
	// }

	public static Boolean isCodingContains(CodeableConcept cc, String system, String code){
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
         for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) && 
				codingDt.getCode().equalsIgnoreCase(code)){
				result = true;
			}
		}
         return result;
	}
	
	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, String code){
		 Boolean result = false;
		 for (CodeableConcept cc : ccList)
			{
				if (isCodingContains(cc, system, code)){
					result = true;
				}
			}
			return result;
	}
	
	
	
	public static Boolean isCodingContains(CodeableConcept cc, String system, Set<String> codes)
	{
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
        for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) && 
				codes.contains(codingDt.getCode()))
			{
				result = true;
			}
		}
        return result;
	}
	
	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, Set<String> codes)
	{
		Boolean result = false;
	 	for (CodeableConcept cc : ccList)
		{
			if (isCodingContains(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}
	
	
	
	
	
	public static Boolean isCodingContains(CodeableConcept cc, String system, String[] codes)
	{
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
        for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) && 
					ArrayUtils.contains(codes, codingDt.getCode()))
			{
				result = true;
			}
		}
        return result;
	}
	
	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, String[] codes)
	{
		Boolean result = false;
	 	for (CodeableConcept cc : ccList)
		{
			if (isCodingContains(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}
	
	
	
	public static Boolean isCodingContains3(org.hl7.fhir.dstu3.model.CodeableConcept cc, String system, String code){
		 Boolean result = false;
		 List<org.hl7.fhir.dstu3.model.Coding> l =  cc.getCoding();
        for (org.hl7.fhir.dstu3.model.Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) && 
				codingDt.getCode().equalsIgnoreCase(code)){
				result = true;
			}
		}
        return result;
	}
	
	public static Boolean isCodingContains3(List<org.hl7.fhir.dstu3.model.CodeableConcept> ccList, String system, String code){
		Boolean result = false;
	 	for (org.hl7.fhir.dstu3.model.CodeableConcept cc : ccList)
		{
			if (isCodingContains3(cc, system, code)){
				result = true;
			}
		}
		return result;
	}
	
	
	
	
	
	public static Boolean isCodingContains3(org.hl7.fhir.dstu3.model.CodeableConcept cc, String system, String[] codes){
		 Boolean result = false;
		 List<org.hl7.fhir.dstu3.model.Coding> l =  cc.getCoding();
		 for (org.hl7.fhir.dstu3.model.Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) && 
			   ArrayUtils.contains(codes, codingDt.getCode())){
				result = true;
			}
		 }
		 return result;
	}
	
	public static Boolean isCodingContains3(List<org.hl7.fhir.dstu3.model.CodeableConcept> ccList, String system, String[] codes){
		Boolean result = false;
		for (org.hl7.fhir.dstu3.model.CodeableConcept cc : ccList)
		{
			if (isCodingContains3(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}
	
	// pre-condition: must match on non-null system and value.  Ignores case.
	public static Boolean isIdentifierListContains(List identifierList, Identifier identifier)
	{
		Boolean result = false;
		
		if((identifier != null) && (identifier.getSystem() != null) && (identifier.getValue() != null))
		{
			for (Identifier identifierListEntry : (List<Identifier>)identifierList) 
			{
    			if((identifierListEntry != null) && (identifierListEntry.getSystem() != null) && (identifierListEntry.getValue() != null) && 
    			   (identifierListEntry.getSystem().equalsIgnoreCase(identifier.getSystem())) && 
    			   (identifierListEntry.getValue().equalsIgnoreCase(identifier.getValue())))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}
	
	// pre-condition: must match on non-null system and value.  Ignores case.
	public static Boolean isIdentifierListContains21(List identifierList, org.hl7.fhir.dstu3.model.Identifier identifier)
	{
		Boolean result = false;
		
		if((identifier != null) && (identifier.getSystem() != null) && (identifier.getValue() != null))
		{
			for (org.hl7.fhir.dstu3.model.Identifier identifierListEntry : (List<org.hl7.fhir.dstu3.model.Identifier>)identifierList) 
			{
    			if((identifierListEntry != null) && (identifierListEntry.getSystem() != null) && (identifierListEntry.getValue() != null) && 
    			   (identifierListEntry.getSystem().equalsIgnoreCase(identifier.getSystem())) && 
    			   (identifierListEntry.getValue().equalsIgnoreCase(identifier.getValue())))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}
	
	public static Boolean isIdentifierListContains3(List identifierList, String id)
	{
		Boolean result = false;
		
		if(id != null)
		{
			for (org.hl7.fhir.dstu3.model.Identifier identifierListEntry : (List<org.hl7.fhir.dstu3.model.Identifier>)identifierList) 
			{
    			if((identifierListEntry != null) && (identifierListEntry.getValue() != null) &&
    			   (identifierListEntry.getValue().equalsIgnoreCase(id)))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}
		
	public static boolean isReferenceListContains(List<org.hl7.fhir.dstu3.model.Reference> referenceList, String id)
	{
		if((referenceList != null) && (id != null))
		{
			for (org.hl7.fhir.dstu3.model.Reference referenceListEntry : referenceList) 
			{
				if(referenceListEntry.getReference() != null)
    			{
					if(referenceListEntry.getReference().equals(id))
	    			{
						return true;
	    			}
    			}
    		}
		 }
		return false;
	}
					

	/*
	 * Returns a person's age using the calendarUnits specified.  If unable to compute, returns -1.
	 *
	 * Time components are ignored if calendarTimeUnit is in years, months, or days.
	 * 
	 * @param calendarTimeUnit - must be in java Calendar units.  Weeks are not supported.
	 * 	
	 */
	public static long getAgeInTimeUnitAtTime(java.util.Date birthDate, java.util.Date specifiedTime, int calendarTimeUnit)
	{
		if ((birthDate == null) || (specifiedTime == null))
		{
			return -1;
		}
	
		DateUtility dateUtility = DateUtility.getInstance();

		// this function ignores time components if time units are days or above
		AbsoluteTimeDifference atd = dateUtility.getAbsoluteTimeDifference(birthDate, specifiedTime, calendarTimeUnit);
	
		if (calendarTimeUnit == java.util.Calendar.YEAR)
		{
			return atd.getYearDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MONTH)
		{
			return atd.getMonthDifference();
		}
		else if ((calendarTimeUnit == java.util.Calendar.DATE) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_MONTH) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_WEEK) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_WEEK_IN_MONTH) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_YEAR))
		{
			return atd.getDayDifference();
		}
		else if ((calendarTimeUnit == java.util.Calendar.HOUR_OF_DAY) ||
                (calendarTimeUnit == java.util.Calendar.HOUR))
		{
			return atd.getHourDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MINUTE)
		{
			return atd.getMinuteDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.SECOND)
		{
			return atd.getSecondDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MILLISECOND)
		{
			return atd.getMillisecondDifference();
		}
		else
		{
			return -1;
		}
	}
	
	public static class Assertion extends Object
	{
		private String myValue;
		
		public Assertion(String value)
		{
			myValue = value;
		}
		
		public String getValue()
		{
			return myValue;
		}
		
		public void setValue(String value)
		{
			myValue = value; 
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}
	
	public static class EvalTime extends Object
	{
		private DateTimeType myValue;

		public EvalTime(DateTimeType value) {
			this.myValue = value;
		}
		
		public DateTimeType getValue() {
			return myValue;
		}

		public void setValue(DateTimeType value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EvalTime other = (EvalTime) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}
	
	public static class EvalTime21 extends Object
	{
		private DateTimeType myValue;

		public EvalTime21(DateTimeType value) {
			this.myValue = value;
		}
		
		public DateTimeType getValue() {
			return myValue;
		}

		public void setValue(DateTimeType value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EvalTime other = (EvalTime) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}
	
	public static class FocalPatientId extends Object
	{
		private Identifier myValue;

		public FocalPatientId(Identifier value) {
			this.myValue = value;
		}
		
		public Identifier getValue() {
			return myValue;
		}

		public void setValue(Identifier value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {  // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FocalPatientId other = (FocalPatientId) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}
	
	public static class FocalPatientId3 extends Object
	{
		private org.hl7.fhir.dstu3.model.Identifier myValue;

		public FocalPatientId3(org.hl7.fhir.dstu3.model.Identifier value) {
			this.myValue = value;
		}
		
		public org.hl7.fhir.dstu3.model.Identifier getValue() {
			return myValue;
		}

		public void setValue(org.hl7.fhir.dstu3.model.Identifier value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {  // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FocalPatientId other = (FocalPatientId) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}
	
	public static class ClientLanguage extends Object
	{
		private String myValue;
		
		public ClientLanguage(String value)
		{
			myValue = value;
		}
		
		public String getValue()
		{
			return myValue;
		}
		
		public void setValue(String value)
		{
			myValue = value; 
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}

	public static class ClientTimeZoneOffset extends Object
	{
		private String myValue;
		
		public ClientTimeZoneOffset(String value)
		{
			myValue = value;
		}
		
		public String getValue()
		{
			return myValue;
		}
		
		public void setValue(String value)
		{
			myValue = value; 
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}
	
	public static class FiredRule extends Object
	{
		private String myValue;
		
		public FiredRule(String value)
		{
			myValue = value;
		}
		
		public String getValue()
		{
			return myValue;
		}
		
		public void setValue(String value)
		{
			myValue = value; 
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}				
	}

	public static class NamedInteger extends Object
	{
		private String myName;
		private Integer myInteger;
		
		public NamedInteger(String name, int integer) {
			this.myName = name;
			this.myInteger = new Integer(integer);
		}
		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Integer getInteger() {
			return myInteger;
		}
		public void setInteger(int integer) {
			this.myInteger = new Integer(integer);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myInteger == null) ? 0 : myInteger.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedInteger other = (NamedInteger) obj;
			if (myInteger == null) {
				if (other.myInteger != null)
					return false;
			} else if (!myInteger.equals(other.myInteger))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}					
	}
	
	public static class NamedDouble extends Object
	{
		private String myName;
		private Double myDouble;
		
		public NamedDouble(String name, double inputDouble) {
			this.myName = name;
			this.myDouble = new Double(inputDouble);
		}
		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Double getDouble() {
			return myDouble;
		}
		public void setDouble(double inputDouble) {
			this.myDouble = new Double(inputDouble);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myDouble == null) ? 0 : myDouble.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedDouble other = (NamedDouble) obj;
			if (myDouble == null) {
				if (other.myDouble != null)
					return false;
			} else if (!myDouble.equals(other.myDouble))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}
	
	public static class NamedBoolean extends Object
	{
		private String myName;
		private Boolean myBoolean;
		
		public NamedBoolean(String name, boolean bool) {
			this.myName = name;
			this.myBoolean = new Boolean(bool);
		}
		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Boolean getBool() {
			return myBoolean;
		}
		public void setBool(boolean bool) {
			this.myBoolean = new Boolean(bool);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myBoolean == null) ? 0 : myBoolean.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedBoolean other = (NamedBoolean) obj;
			if (myBoolean == null) {
				if (other.myBoolean != null)
					return false;
			} else if (!myBoolean.equals(other.myBoolean))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}				
	}
	
	public static class NamedString extends Object
	{
		private String myName;
		private String myString;
		
		public NamedString(String name, String string)
		{
			this.myName = name;
			this.myString = string;
		}
		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public String getString() {
			return myString;
		}
		public void setString(String string) {
			this.myString = string;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			result = prime * result
					+ ((myString == null) ? 0 : myString.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedString other = (NamedString) obj;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			if (myString == null) {
				if (other.myString != null)
					return false;
			} else if (!myString.equals(other.myString))
				return false;
			return true;
		}
	}
	
	
	public static class NamedEncounter extends Object
	{
		private String myName;
		private Encounter myEncounter;
		
		public NamedEncounter(String name, Encounter encounter) {
			this.myName = name;
			this.myEncounter = encounter;
		}
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Encounter getEncounter() {
			return myEncounter;
		}
		public void setEncounter(Encounter encounter) {
			this.myEncounter = encounter;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myEncounter == null) ? 0 : myEncounter.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) { // TODO: Encounter does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedEncounter other = (NamedEncounter) obj;
			if (myEncounter == null) {
				if (other.myEncounter != null)
					return false;
			} else if (!myEncounter.equals(other.myEncounter))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}				
	}
	
	public static class NamedList extends Object
	{
		private String myName;
		private List myList;
		
		public NamedList(String name, List list) {
			this.myName = name;
			this.myList = list;
		}		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public List getList() {
			return myList;
		}
		public void setList(List list) {
			this.myList = list;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myList == null) ? 0 : myList.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedList other = (NamedList) obj;
			if (myList == null) {
				if (other.myList != null)
					return false;
			} else if (!myList.equals(other.myList))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}
	
	public static class NamedDateTimeDt extends Object
	{
		private String myName;
		private DateTimeType myDateTimeDt;
		
		public NamedDateTimeDt(String name, DateTimeType dateTimeDt)
		{
			myName = name;
			myDateTimeDt = dateTimeDt;
		}

		public String getName() {
			return myName;
		}

		public void setName(String name) {
			this.myName = name;
		}

		public DateTimeType getDateTimeDt() {
			return myDateTimeDt;
		}

		public void setDateTimeDt(DateTimeType dateTimeDt) {
			this.myDateTimeDt = dateTimeDt;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myDateTimeDt == null) ? 0 : myDateTimeDt.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedDateTimeDt other = (NamedDateTimeDt) obj;
			if (myDateTimeDt == null) {
				if (other.myDateTimeDt != null)
					return false;
			} else if (!myDateTimeDt.equals(other.myDateTimeDt))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}
	
	public static class NamedIdentifierDt extends Object
	{
		private String myName;
		private Identifier myIdentifierDt;
		
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Identifier getIdentifierDt() {
			return myIdentifierDt;
		}
		public void setIdentifierDt(Identifier identifierDt) {
			this.myIdentifierDt = identifierDt;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((myIdentifierDt == null) ? 0 : myIdentifierDt.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) { // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedIdentifierDt other = (NamedIdentifierDt) obj;
			if (myIdentifierDt == null) {
				if (other.myIdentifierDt != null)
					return false;
			} else if (!myIdentifierDt.equals(other.myIdentifierDt))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}
	
	public static void writeToFile(String outputPath, InputStream inputStream)
	{
		  OutputStream outputStream = null;
		 
			try {
				// write the inputStream to a FileOutputStream
				outputStream = new FileOutputStream(new File(outputPath));
		 
				int read = 0;
				byte[] bytes = new byte[1024];
		 
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
		 	} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (outputStream != null) {
					try {
						// outputStream.flush();
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
		 
				}
			}
	}
	
	public static void writeToFile(String outputPath, ArrayList<String> outputLines)
	{
		  StringBuffer outputStringBuffer = new StringBuffer();;
		  for (int k = 0; k < outputLines.size(); k++)
		  {
			  outputStringBuffer.append(outputLines.get(k));
			  if (k != outputLines.size() - 1)
			  {
				  outputStringBuffer.append("\n");
			  }
		  }
		  InputStream stream = new ByteArrayInputStream(outputStringBuffer.toString().getBytes(StandardCharsets.UTF_8));
		  writeToFile(outputPath, stream);
	}
}
